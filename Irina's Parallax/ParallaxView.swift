//
//  ParallaxView.swift
//  Irina's Parallax
//
//  Created by Vlad Tufis on 17/01/2020.
//  Copyright © 2020 Vlad Tufis. All rights reserved.
//

import Foundation
import UIKit

protocol ParallaxViewDelegate: class {
	
	func didMoveToState(_ view: ParallaxView, state: ParallaxView.State)
	
}

func ==(lhs: ParallaxView.State, rhs: ParallaxView.State) -> Bool {
	
	switch (lhs, rhs) {
	case (.initial(let p1), .initial(let p2)):
		return p1 == p2
	case (.hidden(let p1), .hidden(let p2)):
		return p1 == p2
	case (.visible, .visible):
		return true
	default:
		return false
	}
	
}

class ParallaxView: UIView {
	
	struct AnimationBounds {
		var start: CGFloat
		var end: CGFloat
		
		static let zero = AnimationBounds(start: 0.0, end: 0.0)
	}
	
	enum State {
		
		enum Direction {
			case left
			case right
			case none
		}
		
		case initial(Double)
		case visible
		case hidden(Double)
		
		func newStateFrom(fraction: Double, in direction: Direction, skipLeft: Bool = false, skipRight: Bool = false) -> State {
			switch self {
			case .initial(let p):
				switch p {
				case 1.0:
					return .visible
				case 0.0:
					if direction == .left {
						return self
					}
					return .initial(fraction)
				default:
					if direction == .left {
						return .initial(max(0.0, p - fraction))
					} else {
						return .initial(min(p + fraction, 1.0))
					}
				}
			case .visible:
				if direction == .left {
					if skipLeft {
						return self
					}
					return .initial(1 - fraction)
				} else {
					if skipRight {
						return self
					}
					return .hidden(fraction)
				}
			case .hidden(let p):
				switch p {
				case 0.0:
					return .visible
				case 1.0:
					if direction == .right {
						return self
					}
					return .hidden(1 - fraction)
				default:
					// progress is in the middle\
					if direction == .left {
						return .hidden(max(0.0, p - fraction))
					} else {
						return .hidden(min(p + fraction, 1.0))
					}
				}
			}
		}
	}
	
	weak var delegate: ParallaxViewDelegate?
	
	private var panDirection: State.Direction = .none
	
	// used to detect the pan direction accurately
	private var lastCoordinate: CGFloat = .zero {
		didSet {
			if lastCoordinate == oldValue {
				return
			}
			panDirection = lastCoordinate - oldValue > 0 ? .left : .right
		}
	}
	
	// the state of the ParallaxView
	var state: State = .initial(0.0) {
		didSet {
			if state == oldValue {
				return
			}
			updateFor(state: state, duration: 1.0)
			delegate?.didMoveToState(self, state: state)
		}
	}
	
	private lazy var backgroundView: UIImageView = {
		let imageView = UIImageView()
		imageView.backgroundColor = .clear
		imageView.contentMode = .scaleAspectFit
		return imageView
	}()
	
	private lazy var shapeView: UIImageView = {
		let imageView = UIImageView()
		imageView.backgroundColor = .clear
		imageView.contentMode = .scaleAspectFill
		return imageView
	}()
	
	private lazy var strokeView: UIImageView = {
		let imageView = UIImageView()
		imageView.backgroundColor = .clear
		imageView.contentMode = .scaleAspectFit
		return imageView
	}()
	
	var backgroundBounds: AnimationBounds = .zero {
		didSet {
			backgroundView.translate(offset: backgroundBounds.start)
		}
	}
	
	var strokeBounds: AnimationBounds = .zero {
		didSet {
			strokeView.translate(offset: strokeBounds.start)
		}
	}
	
	var shapeBounds: AnimationBounds = .zero {
		didSet {
			shapeView.translate(offset: shapeBounds.start)
		}
	}
	
	private let firstView: Bool
	
	private let lastView: Bool
	
	private let name: String
	
	init(name: String, background: String, shape: String, stroke: String, lastView: Bool = false, firstView: Bool = false) {
		self.name = name
		self.firstView = firstView
		self.lastView = lastView
		super.init(frame: .zero)
		
		backgroundView.image = UIImage(named: background)!
		shapeView.image = UIImage(named: shape)!
		strokeView.image = UIImage(named: stroke)!
		
		// add the subviews
		addSubview(backgroundView)
		addSubview(shapeView)
		addSubview(strokeView)
		
		// constrain them
		backgroundView.pinToEdges(in: self)
		shapeView.pinToEdges(in: self)
		strokeView.pinToEdges(in: self)
		
		addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didPan)))
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func animate(over duration: TimeInterval) {
		print("\(name).animate")
		UIView.animate(withDuration: duration) { [weak self] in
			guard let self = self else {
				return
			}
			self.adjustTo(alpha: 1.0)
			self.backgroundView.translate(offset: self.backgroundBounds.end)
			self.shapeView.translate(offset: self.shapeBounds.end)
			self.strokeView.translate(offset: self.strokeBounds.end)
			self.state = .visible
		}
	}
	
	func hide(over duration: TimeInterval) {
		print("\(name).hide")
		self.state = .initial(0.0)
//		UIView.animate(withDuration: duration) { [weak self] in
//			guard let self = self else {
//				return
//			}
////			self.adjustTo(alpha: 0.0)
////			self.backgroundView.translate(offset: self.backgroundBounds.start)
////			self.shapeView.translate(offset: self.shapeBounds.start)
////			self.strokeView.translate(offset: self.strokeBounds.start)
//			self.state = .initial(0.0)
//		}
	}
	
	func show(over duration: TimeInterval) {
		print("\(name).show")
		self.state = .visible
//		UIView.animate(withDuration: duration) { [weak self] in
//			guard let self = self else {
//				return
//			}
////			self.adjustTo(alpha: 1.0)
////			self.backgroundView.translate(offset: self.backgroundBounds.end)
////			self.shapeView.translate(offset: self.shapeBounds.end)
////			self.strokeView.translate(offset: self.strokeBounds.end)
//			self.state = .visible
//		}
	}
	
	func updateFor(state: State, duration: TimeInterval) {
		UIView.animate(withDuration: duration) { [weak self] in
			guard let self = self else {
				return
			}
			switch state {
			case .initial(let progress):
				let p = CGFloat(progress)
				self.backgroundView.translate(offset: self.backgroundBounds.start * (1 - p) + self.backgroundBounds.end * p)
				self.shapeView.translate(offset: self.shapeBounds.start * (1 - p) + self.shapeBounds.end * p)
				self.strokeView.translate(offset: self.strokeBounds.start * (1 - p) + self.strokeBounds.end * p)
				self.adjustTo(alpha: p)
			case .hidden(let progress):
				let p = CGFloat(progress)
				self.adjustTo(alpha: 1 - p)
			case .visible:
				self.backgroundView.translate(offset: self.backgroundBounds.end)
				self.shapeView.translate(offset: self.shapeBounds.end)
				self.strokeView.translate(offset: self.strokeBounds.end)
				self.adjustTo(alpha: 1.0)
			}
		}
	}
	
	private func adjustTo(alpha: CGFloat) {
		self.alpha = alpha
		backgroundView.alpha = alpha
		shapeView.alpha = alpha
		strokeView.alpha = alpha
	}
	
	@objc private func didPan(gesture: UIPanGestureRecognizer) {
		let translation = gesture.translation(in: self)
		lastCoordinate = translation.x
		state = state.newStateFrom(fraction: 0.1, in: panDirection, skipLeft: firstView, skipRight: lastView)
		// UNCOMMENT THESE LINES IF YOU WANT THE PAN GESTURE's REAL DELTA TO REFLECT THE ANIMATION
//		let fraction = Double(min(abs(translation.x)/200, 1.0))
//		state = state.newStateFrom(fraction: fraction, in: panDirection)
	}
	
}

extension UIView {
	
	func pinToEdges(left: CGFloat = 0.0, right: CGFloat = 0.0, top: CGFloat = 0.0, bottom: CGFloat = 0.0, in view: UIView) {
		self.topAnchor.constraint(equalTo: view.topAnchor, constant: top).isActive = true
		self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottom).isActive = true
		self.leftAnchor.constraint(equalTo: view.leftAnchor, constant: left).isActive = true
		self.rightAnchor.constraint(equalTo: view.rightAnchor, constant: right).isActive = true
		self.translatesAutoresizingMaskIntoConstraints = false
	}
	
	func translate(offset: CGFloat) {
		transform = CGAffineTransform(translationX: offset, y: 0.0)
	}
	
}

extension Double {
	
	func boundBy(low: Double, high: Double) -> Double {
		return max(low, min(self, high))
	}
	
}
