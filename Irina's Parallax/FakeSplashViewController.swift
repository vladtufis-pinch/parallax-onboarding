//
//  FakeSplashViewController.swift
//  Irina's Parallax
//
//  Created by Vlad Tufis on 17/01/2020.
//  Copyright © 2020 Vlad Tufis. All rights reserved.
//

import Foundation
import UIKit

class FakeSplashViewController: UIViewController {
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: executeTimer)
	}
	
	private func executeTimer(timer: Timer) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let vc = storyboard.instantiateViewController(identifier: "OnboardingViewController")
		vc.modalPresentationStyle = .fullScreen
		vc.modalTransitionStyle = .crossDissolve
		present(vc, animated: true, completion: nil)
	}
	
}
