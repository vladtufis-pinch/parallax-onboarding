//
//  OnboardingViewController.swift
//  Irina's Parallax
//
//  Created by Vlad Tufis on 17/01/2020.
//  Copyright © 2020 Vlad Tufis. All rights reserved.
//

import Foundation
import UIKit

class OnboardingViewController: UIViewController {
	
	private lazy var view1: ParallaxView = {
		return ParallaxView(name: "1", background: "Background-slide1", shape: "Shape-slide1", stroke: "Stroke-slide1", firstView: true)
	}()
	
	private lazy var view2: ParallaxView = {
		return ParallaxView(name: "2", background: "Background-slide2", shape: "Shape-slide2", stroke: "Stroke-slide2")
	}()
	
	private lazy var view3: ParallaxView = {
		return ParallaxView(name: "3", background: "Background-slide3", shape: "Shape-slide3", stroke: "Stroke-slide3", lastView: true)
	}()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setup()
		view1.alpha = 0.0
		view2.alpha = 0.0
		view3.alpha = 0.0
	}
	
	private func setup() {
		view1.delegate = self
		view2.delegate = self
		view3.delegate = self
		view.addSubview(view1)
		view.addSubview(view2)
		view.addSubview(view3)
		
		view1.pinToEdges(in: view)
		view2.pinToEdges(in: view)
		view3.pinToEdges(in: view)
		
		view1.backgroundBounds = ParallaxView.AnimationBounds(start: -20.0, end: 0.0)
		view1.shapeBounds = ParallaxView.AnimationBounds(start: -120.0, end: -70.0)
		view1.strokeBounds = ParallaxView.AnimationBounds(start: view.frame.size.width, end: view.frame.size.width - 120)
		
		view2.backgroundBounds = ParallaxView.AnimationBounds(start: -20.0, end: 0.0)
		view2.shapeBounds = ParallaxView.AnimationBounds(start: view.frame.size.width, end: view.frame.size.width * 0.05)
		view2.strokeBounds = ParallaxView.AnimationBounds(start: view.frame.size.width, end: view.frame.size.width * 0.3)
		
		view3.backgroundBounds = ParallaxView.AnimationBounds(start: -20.0, end: 0.0)
		view3.shapeBounds = ParallaxView.AnimationBounds(start: -170.0, end: -50.0)
		view3.strokeBounds = ParallaxView.AnimationBounds(start: -view.frame.size.width, end: -view.frame.size.width * 0.5)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		view1.show(over: 1.0)
	}
	
}

extension OnboardingViewController: ParallaxViewDelegate {
	
	func didMoveToState(_ view: ParallaxView, state: ParallaxView.State) {
		switch view {
		case view1:
			handleTransitionsForView1()
		case view2:
			handleTransitionsForView2()
			break
		case view3:
			handleTransitionsForView3()
			break
		default:
			// no idea what we are dealing with
			break
		}
	}
	
	private func handleTransitionsForView1() {
		print(view1.state, view2.state, view3.state)
		switch view1.state {
		case .hidden(let p):
			if p >= 0.95 {
//				view1.hide(over: 1.0)
				view2.show(over: 1.0)
			}
		default:
			break
		}
	}
	
	private func handleTransitionsForView2() {
		switch view2.state {
		case .hidden(let p):
			if p >= 0.95 {
//				view2.hide(over: 1.0)
				view3.show(over: 1.0)
			}
		case .initial(let p):
			if p <= 0.0 {
//				view2.hide(over: 1.0)
				view1.show(over: 1.0)
			}
		default:
			break
		}
	}
	
	private func handleTransitionsForView3() {
		switch view3.state {
		case .initial(let p):
			if p <= 0.0 {
//				view3.hide(over: 1.0)
				view2.show(over: 1.0)
			}
		default:
			break
		}
	}
	
}
